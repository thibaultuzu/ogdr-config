package com.fuseim.wells

import java.util.List;
import java.util.Arrays;
import java.util.Map;
import com.fuseim.work.data.model.*;
import com.fuseim.search.model.*;
import com.fuseim.search.service.*;
import com.fuseim.site.service.*;
import com.fuseim.site.framework.XstreamlineHelper;
import com.fuseim.expertadvisor.model.FormField;
import com.fuseim.expertadvisor.model.AdvisorMarker;
import com.fuseim.expertadvisor.action.Advisor;
import com.fuseim.etl.action.SpreadsheetCatalogETL;
import com.fuseim.expertadvisor.model.SpreadsheetErrorMessage;
import com.fuseim.process.action.TaskBean;

global SpreadsheetCatalogETL spreadsheetCatalogETL;
global XstreamlineHelper xstreamline;
global Advisor advisor;


function boolean fieldExists(String fieldName, String value, List items) {
  if (value == null) {
    return true;
  }
  for (Object o : items) {
    CatalogItem item = (CatalogItem) o;
    if (value.equalsIgnoreCase(item.getStringValue(fieldName))) {
      return true;
    }
  }
  return false;
}

function boolean setConcession(CatalogItem itemCat, List items) {
  for (Object o : items) {
    CatalogItem item = (CatalogItem) o;
    if (itemCat.getStringValue("ppdmWellboreName").equalsIgnoreCase(item.getStringValue("ppdmWellboreName"))) {
      itemCat.setValue("ppdmConcession", item.getStringValue("ppdmAreaId"));
      return true;
    }
  }
  itemCat.setValue("ppdmConcession", null);
  return false;
}

function boolean checkEntitlement(String fieldName, String value, String operator, String concession, List wellbores, List wellboresEnt, List concessionsEnt) {
  System.out.println("starting check entitlement function");
  if (operator.equalsIgnoreCase("target") || operator.equalsIgnoreCase("mog")) {
    System.out.format("administrator entitlement", value, operator);
    return true;
  }
  // We check first if operator entitled to concession (quicker)
  for (Object o : concessionsEnt) {
    CatalogItem concEnt = (CatalogItem) o;
    if (concession.equalsIgnoreCase(concEnt.getStringValue("ppdmDataId")) && operator.equalsIgnoreCase(concEnt.getStringValue("ppdmEntitledParty"))) {
      System.out.format("wellbore %s new and concession entitled to %s", value, operator);
      return true;
    }
  }
  // then if not we check it is entitled to the wellbore
  for (Object o : wellbores) {
    CatalogItem wellbore = (CatalogItem) o;
    if (value.equalsIgnoreCase(wellbore.getStringValue(fieldName))) {
      for (Object o1 : wellboresEnt) {
        CatalogItem wellboreE = (CatalogItem) o1;
        // System.out.println("wellboreref: "+wellboreE.getStringValue("ppdmDataId"));
        // if (value.equalsIgnoreCase(wellboreE.getStringValue("ppdmDataId"))) {
        //   System.out.println("operator_match: "+wellboreE.getStringValue("ppdmEntitledParty"));
        // } 
        if (value.equalsIgnoreCase(wellboreE.getStringValue("ppdmDataId")) && operator.equalsIgnoreCase(wellboreE.getStringValue("ppdmEntitledParty"))) {
          System.out.format("wellbore %s already exist and entitled to %s", value, operator);
          return true;
        }
      }
      System.out.format("wellbore %s already exist and not entitled to %s", value, operator);
      return false;
    }
  }
  // reach next line means new wellbore so, check concession entitlement
  System.out.format("wellbore %s new and concession not entitled to %s", value, operator);
  return false;
}

rule "Set up pick list"
salience 1500
  when
  then
    Catalog wellboreEntitlement = xstreamline.getCatalog("ppdmFuseimWellboreEntItem");
    CatalogItemCache.instance().clearCache(wellboreEntitlement.getName());
    SearchEvent wellboreEntitlements = new SearchEvent(wellboreEntitlement);
    wellboreEntitlements.search(new CatalogQuery(), true); // do search and cache result
    insert(wellboreEntitlements);

    Catalog wellbore = xstreamline.getCatalog("ppdmWellbore");
    CatalogItemCache.instance().clearCache(wellbore.getName());
    SearchEvent wellbores = new SearchEvent(wellbore);
    wellbores.search(new CatalogQuery(), true); // do search and cache result
    insert(wellbores);

    Catalog wellboreConcession = xstreamline.getCatalog("ppdmWellboreConcession");
    CatalogItemCache.instance().clearCache(wellboreConcession.getName());
    SearchEvent wellboresConcession = new SearchEvent(wellboreConcession);
    wellboresConcession.search(new CatalogQuery(), true); // do search and cache result
    insert(wellboresConcession);

    Catalog wellboreclone = xstreamline.getCatalog("ppdmWellboreClone");
    CatalogItemCache.instance().clearCache(wellboreclone.getName());
    SearchEvent wellboreclones = new SearchEvent(wellboreclone);
    wellboreclones.search(new CatalogQuery(), true); // do search and cache result
    insert(wellboreclones);

    Catalog concessionEntitlement = xstreamline.getCatalog("ppdmFuseimConcessionEntItem");
    CatalogItemCache.instance().clearCache(concessionEntitlement.getName());
    SearchEvent concessionEntitlements = new SearchEvent(concessionEntitlement);
    concessionEntitlements.search(new CatalogQuery(), true); // do search and cache result
    insert(concessionEntitlements);
end

rule "Strict Checking Wells logs"
  @info(Strict mode, completeness/integrity has total score of 100/100)
  @completeness(100)
  @integrity(100)
  salience 1000
  when
    // SearchEvent(catalog.name == "ppdmProjectWellboreV_NDR_Workbook_Strict" || catalog.name == "ppdmProjectWellboreV_NDR_Workbook_operator");
  then
    advisor.advise(drools, null);
end

rule "Existing Wellbore Check"
  @error(The following wellbores are not stored in the system)
  @integrity(-10)
  when
    wellbores: SearchEvent(catalog.name == "ppdmWellbore")
    item: CatalogItem()
    eval(!(fieldExists("ppdmWellboreName", item.getStringValue("ppdmWellboreName"), wellbores.getHits())))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Existing Wellbore Check Passed"
  @pass(The following wellbores have been found in the system)
  when
    wellbores: SearchEvent(catalog.name == "ppdmWellbore")
    item: CatalogItem()
    eval((fieldExists("ppdmWellboreName", item.getStringValue("ppdmWellboreName"), wellbores.getHits())))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "get taskFieldMap"
  salience 1000
  when
  then
    Map taskVariables = TaskBean.instance().getTi().getVariables();
    insert(taskVariables);
end

rule "Entitlement Check"
  @error(You are not entitled to submit data for the following wellbores)
  @integrity(-10)
  when
    Map(business: this["originalActorBusiness"])
    wellbores: SearchEvent(catalog.name == "ppdmWellbore")
    wellboreEntitlements: SearchEvent(catalog.name == "ppdmFuseimWellboreEntItem")
    concessionEntitlements: SearchEvent(catalog.name == "ppdmFuseimConcessionEntItem")
    item: CatalogItem();
    eval(!checkEntitlement("ppdmUwi", item.getStringValue("ppdmWellboreName"), business.toString(), item.getStringValue("ppdmConcession"), wellbores.getHits(), wellboreEntitlements.getHits(), concessionEntitlements.getHits()))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
    // System.out.println(business);
end

rule "Entitlement Check Passed"
  @pass(You are entitled to submit data for the following wellbores)
  when
    Map(business: this["originalActorBusiness"])
    wellbores: SearchEvent(catalog.name == "ppdmWellbore")
    wellboreEntitlements: SearchEvent(catalog.name == "ppdmFuseimWellboreEntItem")
    concessionEntitlements: SearchEvent(catalog.name == "ppdmFuseimConcessionEntItem")
    item: CatalogItem();
    eval(checkEntitlement("ppdmUwi", item.getStringValue("ppdmWellboreName"), business.toString(), item.getStringValue("ppdmConcession"), wellbores.getHits(), wellboreEntitlements.getHits(), concessionEntitlements.getHits()))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
    // System.out.println(business);
end

rule "Set projectId from wellbore Name"
  salience 1000
  when
    wellboresConcession: SearchEvent(catalog.name == "ppdmWellboreConcession");
    item: CatalogItem()
  then
    setConcession(item, wellboresConcession.getHits());
end

rule "Set uwi from wellborename"
  salience 100
  when
    item: CatalogItem()
  then
    item.setValue("ppdmUwi", item.getValue("ppdmWellboreName"));
end  

rule "Set uwi from wellborename for wellbore clone"
  salience 90
  when
    wellboreclones: SearchEvent(catalog.name == "ppdmWellboreClone")
    item: CatalogItem()
    eval((fieldExists("ppdmWellboreName", item.getStringValue("ppdmWellboreName"), wellboreclones.getHits())))
  then
    String newUwi = item.getStringValue("ppdmWellboreName") + "CH1S";
    item.setValue("ppdmUwi", newUwi);
end  

rule "Set source from operator"
  when
    item: CatalogItem()
  then
    item.setValue("ppdmSource", item.getValue("ppdmOperator"));
end 

rule "Set CurveId from Curve Name"
  when
    item: CatalogItem()
  then
    item.setValue("ppdmCurveId", item.getValue("ppdmCurveName"));
end

// rule "Field existence check"
//   @error(Some fields are not present in the system)
//   @integrity(-10)
//   salience 100
//   when
//     sem: SpreadsheetErrorMessage(fieldName == "ppdmFieldId", message == "Value is not valid");
//   then
//     advisor.advise(drools, sem.getItem(), sem.getFieldName() );
// end  

rule "Invalid Reference Data check"
  @error(Invalid value specified, use the Invalid Values tab to review them)
  @integrity(-10)
  salience 70
  when
    sem: SpreadsheetErrorMessage(message == "Value is not valid");
  then
    advisor.advise(drools, sem.getItem(), sem.getFieldName() );
end

rule "Data Source Check"
  @error(The input data is empty)
  @integrity(-10)
  salience 70
  when
    not CatalogItem()
  then
    advisor.advise(drools, null);
end

rule "Spreadsheet Header Match Check"
  @warn(Input spreadsheet should match columns in target catalog)
  @completeness(-10)
  salience 1000
  when
    not(eval(spreadsheetCatalogETL.isTransferDefinitionComplete()));
  then
    advisor.advise(drools, null);
end
