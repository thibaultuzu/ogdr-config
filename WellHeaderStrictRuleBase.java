package com.fuseim.wells;

import java.util.List;
import java.util.Arrays;
import java.util.Map;
import com.fuseim.work.data.model.*;
import com.fuseim.search.model.*;
import com.fuseim.search.service.*;
import com.fuseim.site.service.*;
import com.fuseim.site.framework.XstreamlineHelper;
import com.fuseim.expertadvisor.model.FormField;
import com.fuseim.expertadvisor.model.AdvisorMarker;
import com.fuseim.expertadvisor.action.Advisor;
import com.fuseim.etl.action.SpreadsheetCatalogETL;
import com.fuseim.expertadvisor.model.SpreadsheetErrorMessage;
import com.fuseim.connector.sql.*;
import com.fuseim.process.action.TaskBean;

global SpreadsheetCatalogETL spreadsheetCatalogETL;
global XstreamlineHelper xstreamline;
global Advisor advisor;

function boolean wellInsideBlock(String fieldName, String value, List items, String lat, String longitude, String crs, String concession) {
  if (value == null) {
    return false;
  }
  for (Object o : items) {
    CatalogItem item = (CatalogItem) o;
    if (value.equalsIgnoreCase(item.getStringValue(fieldName))) {
      //if well already exists and in another concession it will triggered "Well Concession Check" rule base, so we can return true here
      return true;
    }
  }
  if (lat.isEmpty()  || longitude.isEmpty()  || crs.isEmpty() ) {
    return false;
  } 


  String[] block_list = concession.split("Concession ");
  if (block_list.length < 2) {
    return false;
  }
  String block = block_list[1];

  String[] parts0 = crs.split("EPSG:"); 
    if (parts0.length < 2) {
    return false;
  }
  String[] parts1 = parts0[1].split(" - ");
  String crsId = parts1[0];

  
  // System.out.println("lon: "+longitude);
  // System.out.println("lat: "+lat);
  // System.out.println("crsId: "+crsId);
  // System.out.println("block: "+block);
  String request = String.format("select ST_contains(c.the_geom, ST_Transform(ST_GeomFromText('POINT(%s %s)',%s),4326)) as res from concessions c where license1='%s';", longitude, lat, crsId, block);

  // System.out.println("request: "+request);

  DataStoreConnection conn = SiteUtil.findEntityByName("DataStoreConnection", "name", "postgresql");
  JdbcSqlExecutor executor = new JdbcSqlExecutor();

  // System.out.println("conn : "+conn);
  // System.out.println("executor : "+executor);

  List result = executor.executeQuery(request, 1, conn);
  try {
    String res = ((Object[])result.get(0))[0].toString();
    // System.out.println("result : "+res);
    if (res == "false") {
      return false;
    } else if (res == "true") {
      return true;
    }
  } catch( Exception e ){
       return false;
  }
  return false;
}

function boolean testDate(CatalogItem itemCat, String dateField) {
  String date = itemCat.getStringValue(dateField);
  String[] dateSplit = date.split("-");
  if (dateSplit.length != 3) {
    return false;
  }
  System.out.println("date check: good # fields");
  // check length fields
  if (dateSplit[0].length() != 4 || dateSplit[1].length() < 1 || dateSplit[1].length() > 2 ||  dateSplit[2].length() < 1 ||  dateSplit[1].length() > 2) {
    return false;
  }
  System.out.println("date check: good length fields");
  // check range fileds
  // TODO: year not hardcoded
  System.out.println("year: "+dateSplit[0]);
  System.out.println("month: "+dateSplit[1]);
  System.out.println("day: "+dateSplit[2]);
  try {
    if (Integer.parseInt(dateSplit[0]) < 1900 || Integer.parseInt(dateSplit[0]) > 2017 || Integer.parseInt(dateSplit[1]) < 1 || Integer.parseInt(dateSplit[1]) > 12 || Integer.parseInt(dateSplit[2]) < 1 || Integer.parseInt(dateSplit[2]) > 31) {
      return false;
    }
  } catch( Exception e ){
    return false;
  }
  System.out.println("date check: good range fields");
  String timeStamp = date.trim() + " 00:00:00";
  itemCat.setValue(dateField, timeStamp);
  return true;
}


function boolean wellWithinRadius(String fieldName, String value, List items, String lat, String longitude, String crs, Integer dist, String concession) {
  if (value == null) {
    return false;
  }
  for (Object o : items) {
    CatalogItem item = (CatalogItem) o;
    if (value.equalsIgnoreCase(item.getStringValue(fieldName))) {
      //if well already exists and in another concession it will triggered "Well Concession Check" rule base, so we can return true here
      return false;
    }
  }
  if (lat.isEmpty()  || longitude.isEmpty()  || crs.isEmpty() ) {
    return true;
  } 

  String[] parts0 = crs.split("EPSG:"); 
    if (parts0.length < 2) {
      return true;
  }
  String[] parts1 = parts0[1].split(" - ");
  String crsId = parts1[0];

  
  // System.out.println("lon: "+longitude);
  // System.out.println("lat: "+lat);
  // System.out.println("crsId: "+crsId);
  // System.out.println("block: "+block);
  String request = String.format("select exists (select uwi FROM \"ppdmProject_well\" where ST_DWithin(ST_Transform(ST_GeomFromText('POINT(%s %s)',%s),3857), ST_Transform(the_geom,3857), %d))a;", longitude, lat, crsId, dist);
  // System.out.println("request_radius: "+request);

  DataStoreConnection conn = SiteUtil.findEntityByName("DataStoreConnection", "name", "postgresql");
  JdbcSqlExecutor executor = new JdbcSqlExecutor();

  // System.out.println("conn : "+conn);
  // System.out.println("executor : "+executor);

  List result = executor.executeQuery(request, 1, conn);
  try {
    String res = ((Object[])result.get(0))[0].toString();
    // System.out.println("result : "+res);
    if (res == "false") {
      return false;
    } else if (res == "true") {
      return true;
    }
  } catch( Exception e ){
       return true;
  }
  return true;
}

function boolean fieldExists(String fieldName, String value, List items) {
  if (value == null) {
    return true;
  }
  for (Object o : items) {
    CatalogItem item = (CatalogItem) o;
    if (value.equalsIgnoreCase(item.getStringValue(fieldName))) {
      return true;
    }
  }
  return false;
}

function boolean fieldExists2fields(String fieldName1, String fieldName2, String value1, String value2, List items) {
  for (Object o : items) {
    CatalogItem item = (CatalogItem) o;
    if (value1.equalsIgnoreCase(item.getStringValue(fieldName1)) && value2.equalsIgnoreCase(item.getStringValue(fieldName2))) {
      return true;
    }
  }
  return false;
}

// function boolean fieldCrossCheck(String fieldName1, String fieldName2, String value, String value, List items) {
//   // if wellbore exists under other well return false
//   for (Object o : items) {
//     CatalogItem item = (CatalogItem) o;
//     if (value1.equalsIgnoreCase(item.getStringValue(fieldName1)) && !value2.equalsIgnoreCase(item.getStringValue(fieldName2))) {
//       return false;
//     }
//   }
//   return true;
// }

function boolean isFloat( String s ){
   try {
       Double.valueOf( s );
       return true;
   } catch( Exception e ){
       return false;
   }
}

function boolean fieldEquals(String field1, String field2) {
  if (field1.equalsIgnoreCase(field2)) {
    return true;
  } 
  return false;
}

function boolean checkEntitlement(String fieldName, String value, String operator, String concession, List wells, List wellsEnt, List concessionsEnt) {
  if (operator.equalsIgnoreCase("target") || operator.equalsIgnoreCase("mog")) {
    System.out.format("administrator entitlement", value, operator);
    return true;
  }
  // We check first if operator entitled to concession (quicker)
  for (Object o : concessionsEnt) {
    CatalogItem concEnt = (CatalogItem) o;
    if (concession.equalsIgnoreCase(concEnt.getStringValue("ppdmDataId")) && operator.equalsIgnoreCase(concEnt.getStringValue("ppdmEntitledParty"))) {
      // System.out.format("well %s new and concession entitled to %s", value, operator);
      return true;
    }
  }
  // then if not we check it is entitled to the well
  for (Object o : wells) {
    CatalogItem well = (CatalogItem) o;
    if (value.equalsIgnoreCase(well.getStringValue(fieldName))) {
      for (Object o1 : wellsEnt) {
        CatalogItem wellE = (CatalogItem) o1;
        // System.out.println("wellref: "+wellE.getStringValue("ppdmDataId"));
        // if (value.equalsIgnoreCase(wellE.getStringValue("ppdmDataId"))) {
        //   System.out.println("operator_match: "+wellE.getStringValue("ppdmEntitledParty"));
        // } 
        if (value.equalsIgnoreCase(wellE.getStringValue("ppdmDataId")) && operator.equalsIgnoreCase(wellE.getStringValue("ppdmEntitledParty"))) {
          // System.out.format("well %s already exist and entitled to %s", value, operator);
          return true;
        }
      }
      // System.out.format("well %s already exist and not entitled to %s", value, operator);
      return false;
    }
  }
  // reach next line means new well so, check concession entitlement
  // System.out.format("well %s new and concession not entitled to %s", value, operator);
  return false;
}


rule "Set up pick list"
salience 1500
  when
  then
    Catalog well = xstreamline.getCatalog("ppdmWell");
    CatalogItemCache.instance().clearCache(well.getName());
    SearchEvent wells = new SearchEvent(well);
    wells.search(new CatalogQuery(), true); // do search and cache result
    insert(wells);

    Catalog wellField = xstreamline.getCatalog("ppdmWellField");
    CatalogItemCache.instance().clearCache(wellField.getName());
    SearchEvent wellsFields = new SearchEvent(wellField);
    wellsFields.search(new CatalogQuery(), true); // do search and cache result
    insert(wellsFields);

    Catalog wellbore = xstreamline.getCatalog("ppdmWellbore");
    CatalogItemCache.instance().clearCache(wellbore.getName());
    SearchEvent wellbores = new SearchEvent(wellbore);
    wellbores.search(new CatalogQuery(), true); // do search and cache result
    insert(wellbores);

    Catalog wellboreclone = xstreamline.getCatalog("ppdmWellboreClone");
    CatalogItemCache.instance().clearCache(wellboreclone.getName());
    SearchEvent wellboreclones = new SearchEvent(wellboreclone);
    wellboreclones.search(new CatalogQuery(), true); // do search and cache result
    insert(wellboreclones);

    Catalog wellEntitlement = xstreamline.getCatalog("ppdmFuseimWellEntItem");
    CatalogItemCache.instance().clearCache(wellEntitlement.getName());
    SearchEvent wellEntitlements = new SearchEvent(wellEntitlement);
    wellEntitlements.search(new CatalogQuery(), true); // do search and cache result
    insert(wellEntitlements);

    Catalog concessionEntitlement = xstreamline.getCatalog("ppdmFuseimConcessionEntItem");
    CatalogItemCache.instance().clearCache(concessionEntitlement.getName());
    SearchEvent concessionEntitlements = new SearchEvent(concessionEntitlement);
    concessionEntitlements.search(new CatalogQuery(), true); // do search and cache result
    insert(concessionEntitlements);
end

rule "Strict Checking Wells"
  @info(Strict mode, completeness/integrity has total score of 100/100)
  @completeness(100)
  @integrity(100)
  salience 1000
  when
    // SearchEvent(catalog.name == "ppdmProjectWellboreV_NDR_Workbook_Strict" || catalog.name == "ppdmProjectWellboreV_NDR_Workbook_operator");
  then
    advisor.advise(drools, null);
end

rule "Existing Wellbore Check"
  @pass(The following wellbores already exist, their attributes will be updated)
  when
    //item: CatalogItem()
    wellbores: SearchEvent(catalog.name == "ppdmWellbore")
    // wellbore: CatalogItem() from wellbores.hits
    item: CatalogItem()
    eval(fieldExists("ppdmWellboreName", item.getStringValue("ppdmWellboreName"), wellbores.getHits()))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "New Wellbore Check"
  @pass(The following wellbores are new they will be created)
  when
    //item: CatalogItem()
    wellbores: SearchEvent(catalog.name == "ppdmWellbore")
    item: CatalogItem()
    eval(!(fieldExists("ppdmWellboreName", item.getStringValue("ppdmWellboreName"), wellbores.getHits())))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Existing Well Check"
  @pass(The following wells already exist, their attributes will be updated)
  when
    //item: CatalogItem()
    wells: SearchEvent(catalog.name == "ppdmWell")
    // well: CatalogItem() from wells.hits
    item: CatalogItem()
    eval(fieldExists("ppdmUwi", item.getStringValue("ppdmParentUwi"), wells.getHits()))
  then
    advisor.advise(drools, item, "ppdmParentUwi");
end

rule "New Well Check"
  @pass(The following wells are new they will be created)
  when
    //item: CatalogItem()
    wells: SearchEvent(catalog.name == "ppdmWell")
    item: CatalogItem()
    eval(!(fieldExists("ppdmUwi", item.getStringValue("ppdmParentUwi"), wells.getHits())))
  then
    advisor.advise(drools, item, "ppdmParentUwi");
end

rule "Well Name/Wellbore Name similar Check"
  @error(The following wellbores have the same name as the associated well)
  @integrity(-10)
  when
    wellboreclones: SearchEvent(catalog.name == "ppdmWellboreClone")
    item: CatalogItem()
    eval(fieldEquals(item.getStringValue("ppdmWellboreName"), item.getStringValue("ppdmParentUwi")))
    && eval(!(fieldExists("ppdmWellboreName", item.getStringValue("ppdmWellboreName"), wellboreclones.getHits())))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
    // System.out.println("okokokokok");
end

// rule "Well Name/Wellbore Name similar Check" old
//   @error(The following wellbores have the same name as the associated well)
//   @integrity(-10)
//   when
//     wellboreclones: SearchEvent(catalog.name == "ppdmWellboreClone")
//     item: CatalogItem(values["ppdmWellboreName"] == values["ppdmParentUwi"]) 
//     && eval(!(fieldExists("ppdmWellboreName", item.getStringValue("ppdmWellboreName"), wellboreclones.getHits())))
//   then
//     advisor.advise(drools, item, "ppdmWellboreName");
//     // System.out.println("okokokokok");
// end

rule "get taskFieldMap"
when
then
  // we should be able to insert all the task variable like so:

  Map taskVariables = TaskBean.instance().getTi().getVariables();
  insert(taskVariables);

  //String originalActorBusiness = String.valueOf(xstreamline.getTaskField("originalActorBusiness"));
  //Map taskFields = new HashMap();
  //taskFields.set("originalActorBusiness", originalActorBusiness);
  //insert(taskFields);
end

// rule "Operator Check"
//   @warn(The following wells have an operator different from the current user operator)
//   @completeness(-10)
//   when
//     Map(business: this["originalActorBusiness"])
//     item: CatalogItem(values["ppdmOperatorSubmit"] != business.toString())
//   then
//     advisor.advise(drools, item, "ppdmParentUwi");
// end

rule "Entitlement Check"
  @error(You are not entitled to submit data for the following wells)
  @integrity(-10)
  when
    Map(business: this["originalActorBusiness"])
    wells: SearchEvent(catalog.name == "ppdmWell")
    wellEntitlements: SearchEvent(catalog.name == "ppdmFuseimWellEntItem")
    concessionEntitlements: SearchEvent(catalog.name == "ppdmFuseimConcessionEntItem")
    item: CatalogItem();
    eval(!checkEntitlement("ppdmUwi", item.getStringValue("ppdmParentUwi"), business.toString(), item.getStringValue("ppdmConcession"), wells.getHits(), wellEntitlements.getHits(), concessionEntitlements.getHits()))
  then
   advisor.advise(drools, item, "ppdmParentUwi");
    // System.out.println(business);
end

rule "Entitlement Check Passed"
  @pass(You are entitled to submit data for the following wells)
  when
    Map(business: this["originalActorBusiness"])
    wells: SearchEvent(catalog.name == "ppdmWell")
    wellEntitlements: SearchEvent(catalog.name == "ppdmFuseimWellEntItem")
    concessionEntitlements: SearchEvent(catalog.name == "ppdmFuseimConcessionEntItem")
    item: CatalogItem();
    eval(checkEntitlement("ppdmUwi", item.getStringValue("ppdmParentUwi"), business.toString(), item.getStringValue("ppdmConcession"), wells.getHits(), wellEntitlements.getHits(), concessionEntitlements.getHits()))
  then
   advisor.advise(drools, item, "ppdmParentUwi");
    // System.out.println(business);
end

rule "Well Within Concession Check"
  @error(The following wells are not geographically contained in the given Concession)
  @integrity(-10)
  when
    //item: CatalogItem()
    wells: SearchEvent(catalog.name == "ppdmWell")
    item: CatalogItem()
    eval(!(wellInsideBlock("ppdmUwi", item.getStringValue("ppdmParentUwi"), wells.getHits(), item.getStringValue("ppdmSurfaceLatitude"), item.getStringValue("ppdmSurfaceLongitude"),
                            item.getStringValue("omanCoordSysId"), item.getStringValue("ppdmConcession"))))
  then
    advisor.advise(drools, item, "ppdmParentUwi");
end

rule "Well inside 15 m Radius Check"
  @error(Wells already exist within a radius of 15 m of the following wells)
  @integrity(-10)
  when
    wells: SearchEvent(catalog.name == "ppdmWell")
    item: CatalogItem()
    eval((wellWithinRadius("ppdmUwi", item.getStringValue("ppdmParentUwi"), wells.getHits(), item.getStringValue("ppdmSurfaceLatitude"), item.getStringValue("ppdmSurfaceLongitude"),
                            item.getStringValue("omanCoordSysId"), 15, item.getStringValue("ppdmConcession"))))
  then
    advisor.advise(drools, item, "ppdmParentUwi");
end

rule "Well/Wellbore Check"
  @error(The following wellbores already exist under another well)
  @integrity(-10)
  when
    wellbores: SearchEvent(catalog.name == "ppdmWellbore")
    item: CatalogItem()
    eval(fieldExists("ppdmWellboreName", item.getStringValue("ppdmWellboreName"), wellbores.getHits())) &&
    eval(!(fieldExists2fields("ppdmUwi", "ppdmParentUwi", item.getStringValue("ppdmWellboreName"), item.getStringValue("ppdmParentUwi"), wellbores.getHits())))
    // wellbore: CatalogItem() from wellbores.hits
    // item: CatalogItem(values["ppdmWellboreName"] == wellbore.values["ppdmUwi"] && 
      // values["ppdmParentUwi"] != wellbore.values["ppdmParentUwi"])

  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

//TODO: add the crs in the comparison
rule "Well location continuity Check"
  @error(The following wells are already stored with different coordinates)
  @integrity(-10)
  when
    //item: CatalogItem()
    wells: SearchEvent(catalog.name == "ppdmWell")
    well: CatalogItem() from wells.hits
    item: CatalogItem(values["ppdmParentUwi"] == well.values["ppdmUwi"] && 
    (values["ppdmSurfaceLatitude"] != well.values["ppdmSurfaceLatitude"] || values["ppdmSurfaceLatitude"] != well.values["ppdmSurfaceLatitude"]))
  then
    advisor.advise(drools, item, "ppdmParentUwi");
end

rule "Well Spud Date Format Check"
  @error(The following wellbores have a well spud date not in YYYY-MM-DD format)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmWellSpudDate"] != null && values["ppdmWellSpudDate"] != "")
    eval(!testDate(item, "ppdmWellSpudDate"))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
  end

rule "Wellbore Completion Date Format Check"
  @error(The following wellbores have a wellbore completion date not in YYYY-MM-DD format)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmWellboreCompletionDate"] != null && values["ppdmWellboreCompletionDate"] != "")
    eval(!testDate(item, "ppdmWellboreCompletionDate"))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Wellbore Spud Date Format Check"
  @error(The following wellbores have a wellbore spud date not in YYYY-MM-DD format)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmWellboreSpudDate"] != null && values["ppdmWellboreSpudDate"] != "")
    eval(!testDate(item, "ppdmWellboreSpudDate"))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Well location integrity Check"
  @error(The following wells have suspicious coordinates)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmSurfaceLatitude"] > 26.42 || < 14.33 || values["ppdmSurfaceLongitude"] > 63.38 || < 51.99)
  then
    advisor.advise(drools, item, "ppdmParentUwi");
end

rule "Well Field Check"
  @error(The following wells are already stored under different field)
  @integrity(-10)
  when
    wellsFields: SearchEvent(catalog.name == "ppdmwellField")
    wellField: CatalogItem() from wellsFields.hits
    item: CatalogItem(values["ppdmParentUwi"] == wellField.values["ppdmUwi"] && values["ppdmFieldId"] != null && wellField.values["ppdmAreaId"] != null &&
       values["ppdmFieldId"] != wellField.values["ppdmAreaId"])
  then
    advisor.advise(drools, item, "ppdmParentUwi");
end

rule "Missing Well Name Check"
  @error(The following wellbores are missing Well Name field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmParentUwi"] == null || values["ppdmParentUwi"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Concession Check"
  @error(The following wellbores are missing Concession field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmConcession"] == null || values["ppdmConcession"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Wellbore Name Check"
  @error(The following wells are missing Wellborename field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmWellboreName"] == null || values["ppdmWellboreName"] == "")
  then
    advisor.advise(drools, item, "ppdmParentUwi");
end

rule "Missing Well Type Check"
  @warn(The following wellbores are missing Well Type field)
  @completeness(-10)
  when
    item: CatalogItem(values["ppdmWellClassSubmit"] == null || values["ppdmWellClassSubmit"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Wellbore Purpose Check"
  @warn(The following wellbores are missing Wellbore Purpose field)
  @completeness(-10)
  when
    item: CatalogItem(values["ppdmWellborePurposeSubmit"] == null || values["ppdmWellborePurposeSubmit"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Wellbore Bottom Location Check"
  @warn(The following wellbores are missing Wellbore Bottom Location field)
  @completeness(-10)
  when
    item: CatalogItem((values["ppdmBottomHoleLatitude"] == null || values["ppdmBottomHoleLatitude"] == "") &&
     (values["ppdmBottomHoleLongitude"] == null || values["ppdmBottomHoleLongitude"] == ""))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Water Depth Unit"
  @error(The following wellbores are missing Water Depth Unit field)
  @integrity(-10)
  when
    item: CatalogItem((values["ppdmWaterDepth"] != null && values["ppdmWaterDepth"] != "") && (
    values["ppdmWaterDepthOuom"] == null || values["ppdmWaterDepthOuom"] == ""))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Kickoff Depth Unit"
  @error(The following wellbores are missing Kickoff Depth Unit field)
  @integrity(-10)
  when
    item: CatalogItem((values["ppdmKickOffDepth"] != null && values["ppdmKickOffDepth"] != "") && (
    values["ppdmKickoffDepthOuom"] == null || values["ppdmKickoffDepthOuom"] == ""))
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Well Spud Date Check"
  @error(The following wellbores are missing Well Spud Date field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmWellSpudDate"] == null || values["ppdmWellSpudDate"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end


rule "Missing Wellbore Type Check"
  @error(The following wellbores are missing Wellbore Type field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmWellboreClassSubmit"] == null || values["ppdmWellboreClassSubmit"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Operator Check"
  @error(The following wellbores are missing Operator field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmOperatorSubmit"] == null || values["ppdmOperatorSubmit"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Platform Check"
  @error(The following wellbores are missing Rig field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmPlatformName"] == null || values["ppdmPlatformName"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Profile Type Check"
  @error(The following wellbores are missing Wellbore Profile Type field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmWellboreProfileTypeSubmit"] == null || values["ppdmWellboreProfileTypeSubmit"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Well Alias Check"
  @error(The following wellbores are missing Well Alias field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmWellAliasName"] == null || values["ppdmWellAliasName"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Wellbore Alias Check"
  @error(The following wellbores are missing Wellbore Alias field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmWellboreAliasName"] == null || values["ppdmWellboreAliasName"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Field Check"
  @warn(The following wellbores are missing Field Name field)
  @completeness(-10)
  when
    item: CatalogItem(values["ppdmFieldId"] == null || values["ppdmFieldId"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Wellbore Status Check"
  @error(The following wellbores are missing Well Status field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmWellboreStatusSubmit"] == null || values["ppdmWellboreStatusSubmit"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Wellbore Content Check"
  @error(The following wellbores are missing Well Show Type field)
  @integrity(-10)
  when
    item: CatalogItem(values["ppdmShowTypeSubmit"] == null || values["ppdmShowTypeSubmit"] == "")
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Well Locations Check"
  @error(The following wellbores are missing surface location information)
  @completeness(0)
  @integrity(-10)
  salience 10
  when
    item: CatalogItem(populatedValues["ppdmSurfaceLongitude"] == null || populatedValues["ppdmSurfaceLatitude"] == null 
      || populatedValues["ppdmSurfaceLongitude"] == "" || populatedValues["ppdmSurfaceLatitude"] == "");
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing CRS Check"
  @error(The following wellbores are missing surface location information)
  @completeness(0)
  @integrity(-10)
  salience 10
  when
    item: CatalogItem(populatedValues["omanCoordSysId"] == null || populatedValues["omanCoordSysId"] == "");
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Projected Well Locations (X/Y) Check"
  @warn(The following wellbores are missing surface location (x/y) information)
  @completeness(-10)
  when
    item: CatalogItem(populatedValues["ppdmSurfaceX"] == null || populatedValues["ppdmSurfaceY"] == null);
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Projected CRS Check"
  @error(The following wellbores are missing projected CRS information)
  @integrity(-10)
  when
    item: CatalogItem(populatedValues["ppdmSurfaceX"] != null && (populatedValues["omanProjCoordSysId"] == null 
      || populatedValues["omanProjCoordSysId"] == ""));
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Depth Refererence Check"
  @error(The following wellbores are missing Depth Reference field)
  @integrity(-10)
  salience 10
  when
    item: CatalogItem(populatedValues["ppdmDepthDatum"] == null || populatedValues["ppdmDepthDatum"] == "");
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Depth Reference Elevation Check"
  @error(The following wellbores are missing Depth Reference Elevation field)
  @integrity(-10)
  salience 10
  when
    item: CatalogItem(populatedValues["ppdmDepthDatumElev"] == null || populatedValues["ppdmDepthDatumElev"] == "");
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Depth Reference Elevation Unit Check"
  @error(The following wellbores are missing Depth Reference Elevation Unit field)
  @integrity(-10)
  salience 10
  when
    item: CatalogItem(populatedValues["ppdmDepthDatumElevOuom"] == null || populatedValues["ppdmDepthDatumElevOuom"] == "");
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Depth Reference Datum Check"
  @error(The following wellbores are missing Depth Reference Datum field)
  @integrity(-10)
  salience 10
  when
    item: CatalogItem(populatedValues["ppdmElevRefDatum"] == null || populatedValues["ppdmElevRefDatum"] == "");
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Total Depth Check"
  @error(The following wellbores are missing Total Depth field)
  @completeness(0)
  @integrity(-10)
  salience 10
  when
    item: CatalogItem(populatedValues["ppdmTotalDepth"] == null || populatedValues["ppdmTotalDepth"] == "");
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Total Depth Unit Check"
  @error(The following wellbores are missing Total Depth Unit field)
  @completeness(0)
  @integrity(-10)
  salience 10
  when
    item: CatalogItem(populatedValues["ppdmTotalDepthOuom"] == null || populatedValues["ppdmTotalDepthOuom"] == "");
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Total Vertical Depth Check"
  @error(The following wellbores are missing Total Vertical Depth field)
  @completeness(0)
  @integrity(-10)
  salience 10
  when
    item: CatalogItem(populatedValues["ppdmMaxTvd"] == null || populatedValues["ppdmMaxTvd"] == "");
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Missing Total Vertical Depth Unit Check"
  @error(The following wellbores are missing Total Vertical Depth Unit field)
  @completeness(0)
  @integrity(-10)
  salience 10
  when
    item: CatalogItem(populatedValues["ppdmMaxTvdOuom"] == null || populatedValues["ppdmMaxTvdOuom"] == "");
  then
    advisor.advise(drools, item, "ppdmWellboreName");
end

rule "Invalid Reference Data check"
  @error(Invalid value specified, use the Invalid Values tab to review them)
  @integrity(-10)
  salience 70

  when
    sem: SpreadsheetErrorMessage(message == "Value is not valid");
  then
    advisor.advise(drools, sem.getItem(), sem.getFieldName() );
end

rule "Data Source Check"
  @error(The input data is empty)
  @integrity(-10)
  salience 70
  when
    not CatalogItem()
  then
    advisor.advise(drools, null);
end

rule "Spreadsheet Header Match Check"
  @warn(Input spreadsheet should match columns in target catalog)
  @completeness(-10)
  salience 1000

  when
    not(eval(spreadsheetCatalogETL.isTransferDefinitionComplete()));
  then
    advisor.advise(drools, null);
end
